import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';

import { rootReducer } from './reducer';
import { ReduxComponent } from './redux.component';
import { CounterComponent } from './counter.component'

console.log(rootReducer)

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot(rootReducer)
  ],
  declarations: [ReduxComponent, CounterComponent]
})
export class ReduxyModule { }
