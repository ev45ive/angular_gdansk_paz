import { Component, OnInit, Input, ChangeDetectionStrategy, DoCheck} from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState, inc,dec } from './reducer';

@Component({
  selector: 'counter',
  template: `
    <p> {{ counter }} </p>
    <button (click)="inc()">Inc</button>
    <button (click)="dec()">Dec</button>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: []
})
export class CounterComponent implements OnInit {

  @Input('counter')
  counter

  ngDoCheck(changes){
    console.log(changes)
  }

  inc(){
    this.store.dispatch(inc())
  }
  dec(){
    this.store.dispatch(inc())
  }
  constructor(private store: Store<AppState>) {

    // setInterval(()=>{
    //   // this.store.dispatch(inc())
    // },500)

  }

  ngOnInit() {
  }

}
