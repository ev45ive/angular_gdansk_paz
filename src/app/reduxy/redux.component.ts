import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState, inc,dec, addItem } from './reducer';

@Component({
  selector: 'redux',
  template: `
    <p>
      redux Works!
    </p>
    <counter [counter]="counter$ | async"></counter>

    <ul><li *ngFor="let item of (items$ | async)">{{item}}</li></ul>

    <input (keyup.enter)="addItem($event.target.value)">
  `,
  styles: []
})
export class ReduxComponent implements OnInit {

  counter$
  items$

  constructor(private store: Store<AppState>) {
    this.counter$ = this.store.select('counter')
    this.items$ = this.store.select('items')
  }
  addItem(message){
    this.store.dispatch(addItem(message))
  }

  ngOnInit() {
  }

}
