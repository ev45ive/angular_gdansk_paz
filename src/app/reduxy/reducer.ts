import { Action, combineReducers } from '@ngrx/store';


export function counterReducer(state: number = 0, action) {
	switch (action.type) {
		case 'INCREMENT':
			return state + 1;

		case 'DECREMENT':
			return state - 1;

		case 'RESET':
			return 0;

		default:
			return state;
	}
}

export function itemsReducer(state = [], action) {
	switch (action.type) {
		case 'ADD_ITEM':
			return [...state, action.payload]

		case 'RESET':
			return [];

		default:
			return state;
	}
}

export interface AppState{
    counter:number
    items: Array<any>
}

const initialState:AppState = {
    counter:0,
    items:[]
}

export const inc = (payload = 1) => ({
    type:'INCREMENT', payload
})
export const dec = (payload = 1) => ({
    type:'DECREMENT', payload
})
export const addItem = (payload) => ({
    type:'ADD_ITEM', payload
})


// export function rootReducer(state = initialState, action){
//     state.counter = counterReducer(state.counter, action)
// }

//export function rootReducer(state = initialState,action:Action){
 //   const reducer = combineReducers(
export const rootReducer = {
        counter: counterReducer,
        items: itemsReducer
    }
// )
//     return reducer(state,action)
// }
