import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'testing',
  template: `
    <p>{{message}}</p>
    <input [(ngModel)]="message">
  `,
  styles: []
})
export class TestingComponent implements OnInit {

  message = 'placki'

  constructor(@Inject('X') public X) { }

  ngOnInit() {
  }

}
