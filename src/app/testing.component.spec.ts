import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingComponent } from './testing.component';
import { By } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

fdescribe('TestingComponent', () => {
  let XMock = true
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestingComponent ],
      imports:[
        FormsModule
      ],
      providers:[
        {provide: 'X', useValue:XMock}
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render text', () => {
    expect(fixture
      .debugElement
      .query(By.css('p'))
      .nativeElement.textContent).toEqual('placki')
  });

  it('should render updated text', () => {
    component.message = "updated"
    fixture.detectChanges()
    expect(fixture
      .debugElement
      .query(By.css('p'))
      .nativeElement.textContent).toEqual('updated')
  });

  it('should updated message on user input', () => {
    let input = fixture
    .debugElement
    .query(By.css('input'))
    
    input.nativeElement.value = "updated"
    //input.triggerEventHandler('input',{ })
    input.nativeElement.dispatchEvent(new Event('input'))

    expect(component.message).toEqual('updated')
  });


  it('should update input message', (done) => {
    component.message = "updated"

    fixture.detectChanges()   
     let input = fixture
        .debugElement
        .query(By.css('input'))
        
        fixture.whenStable().then(()=>{
          expect(input.nativeElement.value).toEqual('updated')
          done()
        })
  });

  it('should use X service', () => {
    expect(component.X).toBe(XMock);
  });
});
