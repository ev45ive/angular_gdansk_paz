import { RouterModule, Routes, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, NavigationCancel, Resolve } from '@angular/router'
import { ModuleWithProviders, Injectable } from '@angular/core';
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';
import { PlaylistsService } from './playlists/playlists.service';
import { ReduxComponent } from './reduxy/redux.component';

@Injectable()
export class CanPlaylists implements CanActivate {
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return true
    }
}


const routes: Routes = [
    { path: '', redirectTo: 'playlists', pathMatch: 'full' },
    { path: 'redux', component: ReduxComponent },
    { path: 'music', loadChildren: './music-search/music-search.module#MusicSearchModule' },
    {
        path: 'playlists', component: PlaylistsComponent, children: [
            { path: '', component: PlaylistContainerComponent },
            { path: 'show/:id', component: PlaylistContainerComponent }
        ],
        canActivate: [CanPlaylists],
        data: {
            title: 'Playlists'
        },
        resolve: {
            'playlist': PlaylistsService
        }
    },
    { path: '**', redirectTo: 'music' },
]


export const RoutingModule = RouterModule.forRoot(routes, {
    // enableTracing: true,
    // useHash: true
})