import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef, ComponentFactoryResolver } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistsListComponent } from './playlists/playlists-list.component';
import { PlaylistDetailsComponent } from './playlists/playlist-details.component';
import { PlaylistItemComponent } from './playlists/playlist-item.component';
import { ModalComponent } from './playlists/modal.component';
import { HighlightDirective } from './playlists/highlight.directive';
import { ModalButtonDirective } from './playlists/modal-button.directive';
import { UnlessDirective } from './playlists/unless.directive';
import { AuthService } from './auth/auth.service';
import { Http, HttpModule } from '@angular/http';
import { PlaylistsService } from './playlists/playlists.service';
import { RoutingModule, CanPlaylists } from './app.routing';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';
import { TestingComponent } from './testing.component';
import { ReduxyModule } from './reduxy/reduxy.module';

export function zrobPlacki(sos){
  return 'placki, sos ' + sos
}

@NgModule({
  declarations: [
    AppComponent,
    PlaylistsComponent,
    PlaylistsListComponent,
    PlaylistDetailsComponent,
    PlaylistItemComponent,
    ModalComponent,
    HighlightDirective,
    ModalButtonDirective,
    UnlessDirective,
    PlaylistContainerComponent,
    TestingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RoutingModule,
    ReduxyModule
  ],
  providers: [
    CanPlaylists,
    PlaylistsService,
    { provide:'sos', useValue: 'malinowy' },
    { provide: 'placki', useFactory: zrobPlacki, deps:['sos'] },
    // { provide: AbstractAuthService, useClass: AuthService  },
    // { provide: AuthService, useClass: AuthService  },
    AuthService,
    // Http,
  ],
  entryComponents:[AppComponent]
  //bootstrap: [AppComponent]
})
export class AppModule {

  ngDoBootstrap(){
    let factory = this.factory.resolveComponentFactory(AppComponent)
    // setTimeout(()=>{
      this.app.bootstrap(factory)
    // },3000)
  }

  constructor(
      private factory:ComponentFactoryResolver,
      private app:ApplicationRef,
      private auth:AuthService
  ){
    this.auth.getToken()
  }

}
