import { Directive, TemplateRef,SimpleChanges, ViewContainerRef, Input, OnChanges, ViewRef } from '@angular/core';

@Directive({
  selector: '[unless]'
})
export class UnlessDirective implements OnChanges {

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes)
  }
  
  cached:ViewRef

  @Input()
  set unless(hide){
    if(hide){
      // this.vcr.clear()
      this.cached = this.vcr.detach()
    }else{

      if(this.cached){
        this.vcr.insert(this.cached)
      }else{
        this.vcr.createEmbeddedView(this.tpl,{
          $implicit: 'ala ma kota',
          dodatkowe: 1234
        })
      }
    }
  }

  @Input('unlessElse')
  else

  constructor(private vcr:ViewContainerRef,
              private tpl: TemplateRef<any>) {
                // console.log(this)
  }

}
