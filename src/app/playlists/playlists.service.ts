import { Injectable, InjectionToken } from '@angular/core';
import { Playlist } from './playlist';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/of'
import 'rxjs/add/operator/delay'
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class PlaylistsService implements Resolve<Playlist> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    return this.getPlaylist( parseInt(route.firstChild.params['id']) )
  }

  constructor() { }

  getPlaylists(){
    return Observable.of(this.playlists)
  }

  getPlaylist(id){
    return Observable.of(this.playlists.find( playlist => 
      playlist.id == id
    ))
  }

  playlists: Playlist[] = [
    {
      id: 1,
      name: "Angular greatest Hits",
      favourite: false,
      color: "#ff0000",
    },
    {
      id: 2,
      name: "Best of Angular",
      favourite: true,
      color: "#00ff00",
    },
    {
      id: 3,
      name: "Angular TOP20",
      favourite: false,
      color: "#0000ff",
    }
  ]

}
