import { Component, OnInit } from '@angular/core';
import { Playlist } from './playlist';
import { PlaylistsService } from './playlists.service';
import { Router, ActivatedRoute } from '@angular/router';

// <ng-template [ngIf]="selected" [ngIfElse]="noSelected"...

@Component({
  selector: 'playlists',
  template: `
    <div class="row">
      <div class="col">
        <playlists-list 
            [playlists]="playlists$ | async" 
            (selectedChange)="select($event)"
            [selected]="selected$ | async">
        </playlists-list>
      </div>
      <div class="col">
        <router-outlet></router-outlet>
      </div>
    </div>    
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {

  selected$

  playlists$

  constructor(private service:PlaylistsService,
              private route:ActivatedRoute,
              private router:Router) {
    this.playlists$ = service.getPlaylists()
    this.selected$ = this.route.data.do(x => console.log(x))
  }

  select(playlist){
    this.router.navigate(['playlists','show',playlist.id])
  }

  ngOnInit() {
    
    // this.selected = this.playlists[0]
  }

}
