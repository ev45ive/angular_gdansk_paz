import { Component, OnInit, Input, ViewChild, ViewChildren, QueryList} from '@angular/core';
import { Playlist } from './playlist';
import { ModalComponent } from './modal.component';

@Component({
  selector: 'playlist-details',
  template: `
  <modal *ngIf="mode == 'show'" [title]="playlist.name">
    <div class="form-group">
      <label>Name: </label>
      <div class="form-control-static"> {{playlist.name}} </div>
    </div>
    <div class="form-group">
      <label>Favourite: </label>
      <div class="form-control-static"> {{ playlist.favourite? 'Favouite' : 'Nope' }} </div>
    </div>
    <div class="form-group">
      <label>Color: </label>
      <div class="form-control-static" 
        [style.backgroundColor]="playlist.color"> {{ playlist.color }} </div>
    </div>
    <footer>
      <button class="btn btn-success float-right" (click)="edit()">Edit</button>  
    </footer> 
  </modal>

  <modal  #modalRef="Modal" *ngIf="mode == 'edit'"  [title]="'Edit ' + playlist.name">
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" [(ngModel)]="playlist.name"
          #nameRef>
    </div>
    <div class="form-group">
        <label>Favourite</label>
        <input type="checkbox" [(ngModel)]="playlist.favourite">
    </div>
    <div class="form-group">
        <label>Color</label>
        <input type="color" [(ngModel)]="playlist.color">
    </div>
    <footer>
      <button modalButton class="btn btn-success float-right" (click)="save()">Save</button>
      <button class="btn btn-danger float-right" (click)="reset(); modalRef.close()">Cancel</button>
    </footer> 
  </modal>
  `,
  // inputs:[
  //   'playlist:playlist'
  // ],
  styles: []
})
export class PlaylistDetailsComponent implements OnInit {


  @Input()
  playlist: Playlist

  mode = 'edit'

  edit() {
    this.mode = 'edit'
  }

  save() {
    console.log('save')
  }

  reset() {
    this.mode = 'show'
  }

  constructor() { }

  @ViewChildren(ModalComponent,{read: ModalComponent})
  modals = new QueryList()

  @ViewChild('nameRef')
  nameField

  ngOnInit() {
    console.log(this)
  }

}
