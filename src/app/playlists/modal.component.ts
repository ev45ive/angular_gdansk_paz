import { Component, OnInit, Input, AfterContentInit, AfterViewInit, ContentChild, ContentChildren, QueryList } from '@angular/core';
import { ModalButtonDirective } from './modal-button.directive';

@Component({
  selector: 'modal',
  exportAs: 'Modal',
  template: `
   <div class="card">
    <div class="card-body">
        <h4 class="card-title">{{title}}</h4>
        <ng-content></ng-content>
    </div>
    <div class="card-footer">
      <ng-content select="footer, .footer"></ng-content>      
    </div>
   </div>
  `,
  styles: []
})
export class ModalComponent implements OnInit, AfterContentInit, AfterViewInit {
  ngAfterViewInit() {
  }
  ngAfterContentInit() {
    console.log(this.buttons)
  }

  @Input()
  title

  @ContentChildren(ModalButtonDirective,{read:ModalButtonDirective,descendants:true})
  buttons = new QueryList()



  close(){
    console.log('close')
  }

  constructor() { }

  ngOnInit() {
  }

}
