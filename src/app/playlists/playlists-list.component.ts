import { Component, OnInit,ViewEncapsulation, Input, EventEmitter, Output } from '@angular/core';
import { Playlist } from './playlist';

// <ng-template [ngFor]="let playlist" [ngForOf]="playlists"></ng-template>

// (mouseenter)=" hover = playlist "
// (mouseleave)=" hover = false "
// [style.border-left-color]="(hover == playlist? playlist.color : 'inherit')"

// [ngFor]="let playlist" [ngForOf]="playlists"
@Component({
  selector: 'playlists-list',
  template: `
    <div class="list-group">
      <div class="list-group-item" 
          *ngFor="let playlist of playlists; let i = index"
          [class.active]="selected == playlist"
          

          [ngClass]="{hover:true}"

          [highlight]="playlist.color"

          (click)="select(playlist)">
        {{i+1}}. {{playlist.name}}
      </div>
    </div>
  `,
  encapsulation: ViewEncapsulation.Emulated,
  styles: [`
    .list-group-item.hover{
      border-left:15px solid black;
    }
  `]
})
export class PlaylistsListComponent implements OnInit {
  
  @Input()
  selected:Playlist

  @Input('playlists')
  playlists: Playlist[] = []

  @Output('selectedChange')
  selectedChange = new EventEmitter<Playlist>()

  select(playlist){
    this.selectedChange.emit(playlist)
  }

  constructor() { }

  ngOnInit() {
  }

}
