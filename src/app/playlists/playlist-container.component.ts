import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from './playlists.service';
import { ActivatedRoute } from '@angular/router';

import 'rxjs/add/operator/pluck'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/switchMap'

@Component({
  selector: 'playlist-container',
  template: `
  <playlist-details *ngIf="selected$ | async as selected; else elseRef" [playlist]="selected"></playlist-details>
  <ng-template #elseRef>Please select playlist</ng-template>
  `,
  styles: []
})
export class PlaylistContainerComponent implements OnInit {

  selected$

  constructor(private service:PlaylistsService, private route: ActivatedRoute) { }

  ngOnInit() {
    // let id = parseInt(this.route.snapshot.params['id'])

    this.selected$ = this.route.params
              // .pluck('id')
              .map(params => parseInt(params['id']))
              .switchMap(id => this.service.getPlaylist(id))    
  }

}
