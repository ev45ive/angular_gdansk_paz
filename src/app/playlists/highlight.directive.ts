import { Directive, ElementRef, OnInit, Attribute, Input, HostListener, Renderer2, HostBinding } from '@angular/core';
import { NgClass } from '@angular/common';

@Directive({
  selector: '[highlight]',
  // host:{
  //   '(click)':'myClick($event)',
  //   '[style.border-left-color]':'getColor()'
  // }
})
export class HighlightDirective implements OnInit {
  
  @HostBinding('class.hover')
  hover;


  // @HostBinding('ngClass')
  // test = {
  //   test:true
  // };
  
  @Input('highlight')
  highlightColor = '#00000'
  
  @HostBinding('style.border-left-color')
  get getColor(){
    // console.log('teraz !')
    return this.hover? this.highlightColor : 'inherit'
  }  
  
  @HostListener('mouseenter',['$event.clientX'])
  onEnter(x:number){
    this.hover = true;
    // this.color = this.highlightColor;
    // this.elem.nativeElement.style.borderLeftColor = this.color
    // this.renderer.setStyle(this.elem.nativeElement,'borderLeftColor',this.color)
  }
  
  @HostListener('mouseleave')
  onExit(){
    this.hover = false;
    // this.color = 'inherit'
    // this.elem.nativeElement.style.borderLeftColor = 'inherit'
    // this.renderer.setStyle(this.elem.nativeElement,'borderLeftColor','inherit')
  }
  
  ngOnInit() {
  //this.elem.nativeElement.style.borderLeftColor = this.color
  }
  constructor(
    private ngClass:NgClass,
    private elem:ElementRef,
    private renderer:Renderer2
    // @Attribute('highlight') private color
  ) {

    // console.log(this, elem)
  }  
}

// console.log(HighlightDirective)