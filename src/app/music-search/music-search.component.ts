import { Component, OnInit } from '@angular/core';
import { MusicService } from './music.service';

@Component({
  selector: 'music-search',
  template: `
  <div class="row">
    <div class="col">
      <search-form [query]="queries$ | async" (queryChanges)="search($event)"></search-form>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <albums-list></albums-list>
    </div>
  </div>
  `,
  styles: []
})
export class MusicSearchComponent implements OnInit {

  constructor(private service: MusicService) {
    this.queries$ = service.queries$
  }
  queries$;

  search(query){
    this.service.search(query)
  }

  ngOnInit() {
  }

}
