import { Component, OnInit, Input, Output } from '@angular/core';
import { FormGroup, AbstractControl, FormControl, FormArray, FormBuilder, Validators, ValidatorFn, ValidationErrors, AsyncValidatorFn } from '@angular/forms';

import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/skipWhile'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/debounceTime'
import { MusicService } from './music.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'search-form',
  template: `
  <form [formGroup]="queryForm">
    <div class="input-group">
      <input type="text" class="form-control" formControlName="query">
    </div>
    <div *ngIf="queryForm.pending">Please wait... </div>
    <ng-container *ngIf="queryForm.controls.query.touched || queryForm.controls.query.dirty">
      <div *ngIf="queryForm.controls.query.errors?.required">Field is required</div>
      <div *ngIf="queryForm.controls.query.errors?.badword">Cant use words like "{{queryForm.controls.query.errors?.badword}}"</div>
      <div *ngIf="queryForm.controls.query.errors?.minlength">
        Field must have at least {{queryForm.controls.query.errors?.minlength.requiredLength}} characters
      </div>
    </ng-container>
  </form>
  `,
  styles: [`
    form .ng-invalid.ng-touched,
    form .ng-invalid.ng-dirty {
      border: 2px solid red;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  queryForm: FormGroup

  constructor(private builder: FormBuilder) {

    const censor = (badword):ValidatorFn => {
      return (control:AbstractControl) => {
        let hasError = control.value.indexOf(badword) !== -1
        return hasError? <ValidationErrors>{ "badword": badword } : null;
      }
    }

    const asyncCensor = (badword):AsyncValidatorFn => {
      return (control:AbstractControl) => {
        return Observable.create( observer => {
          setTimeout(()=>{
            let hasError = control.value.indexOf(badword) !== -1
            let errors = hasError? <ValidationErrors>{ "badword": badword } : null

            observer.next(errors)
            observer.complete()
          },2000)
        })
      }
    }

    this.queryForm = this.builder.group({
      'query': this.builder.control('', [
        Validators.required,
        Validators.minLength(3),
      ],[
        asyncCensor('batman')
      ]),
    })
    window['form'] = this.queryForm;

    // let invalid$ = this.queryForm.statusChanges.filter( status => status != "VALID")

    this.queryChanges = this.queryForm
      .get('query')
      .valueChanges

      // .debounceTime(400)
  }

  @Input()
  set query(query) {
    this.queryForm.get('query').setValue(query)
  }

  @Output()
  queryChanges;

  ngOnInit() {

  }


}
