import { Component, OnInit } from '@angular/core';
import { Album } from './interfaces'
import { MusicService } from './music.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'albums-list',
  template: `
  <div class="card-group" *ngIf="albums$ | async as albums"> 
    <album-item class="card" [album]="album"
      *ngFor="let album of albums; trackBy myTrack"></album-item>
  </div>
  <div *ngIf="!albums?.length"> No results </div>
  `,
  styles: [`
    .card{
      min-width:25%;
      max-width:25%;  
    }
  `]
})
export class AlbumsListComponent implements OnInit {

  albums
  
  albums$
  
  constructor(private service:MusicService) { }
  
  
  ngOnInit() {
    this.albums$ = this.service.getAlbums()
  }
  
  
  myTrack(index,item){
    return item.id
  }
}
