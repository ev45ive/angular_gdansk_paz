interface Entity{
    id: string;
}
interface Named{
    name: string;
}

export interface Album extends Entity, Named{
    images: AlbumImage[];
    artists: Artist[];
}

export interface Artist extends Entity, Named{}

export interface AlbumImage{
    height: number;
    width: number;
    url: string
}

// index.ts
// export * from './interfaces'