import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumsListComponent } from './albums-list.component';
import { AlbumItemComponent } from './album-item.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MusicRoutingModule } from './music-search.routing';
import { MusicService } from './music.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MusicRoutingModule
  ],
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumsListComponent,
    AlbumItemComponent
  ],
  providers:[
    MusicService,
  ],
  exports:[
    // MusicSearchComponent
  ]
})
export class MusicSearchModule { }
