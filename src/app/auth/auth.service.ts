import { Injectable, Inject } from '@angular/core';
import { RequestOptions } from '@angular/http';

@Injectable()
export class AuthService {

  constructor(private requestOptions:RequestOptions) {
    this.requestOptions.headers
    .append('Authorization','Bearer ' + this.getToken())
  }

  getToken(){
      let token = JSON.parse(localStorage.getItem('token'))

      if(!token){
        let match = window.location.hash.match(/#access_token=(.*?)&/)
        token = match && match[1]
      }
      
      if(!token){
        this.authorize()
      }else{
        localStorage.setItem('token', JSON.stringify(token))
        return token;
      }
  }

  // go get some token
  authorize(){
    localStorage.removeItem('token')

    let client_id = '66c9221ed018406f8af4ff75eff588ed'
    let redirect_id = 'http://localhost:4200/'
    let url = `https://accounts.spotify.com/authorize?`
      + `client_id=${client_id}`
      + `&response_type=token`
      + `&redirect_uri=${redirect_id}`

    window.location.replace(url)

  }

}
